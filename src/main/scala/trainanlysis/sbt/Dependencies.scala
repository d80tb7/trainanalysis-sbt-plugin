/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
 
package trainanlysis.sbt

import sbt._

/*
 * Default Versions of dependencies to be used in Train Analysis Apps.
 * Note that these can be overridden at the project level if need be
 */
object Dependencies {

   val scalaLoggingVersion = "3.1.0"
   val logbackVersion = "1.1.3"
   val typesafeConfigVersion = "1.3.0"
   val jason4sVersion = "3.3.0"
   val scalatraVersion = "2.4.0"
   val sparkVersion = "1.6.1"
   val cassandraConnectorVersion = "1.6.0"
   val woodstoxVersion = "5.0.2"
   val scalatestVersion = "2.2.0"
   val scalamockVersion = "3.2.2"
   val phatomVersion = "1.22.0"
   val servletApiVersion = "3.1.0"
   val jettyVersion = "9.2.14.v20151106"
   val akkaVersion = "2.3.4"
   val dispatchversion = "0.11.1"
   val quartzVersion = "2.2.2"
   val commonsNetVersion = "3.4"
   val commonsIoVersion = "2.5"
   val jacksonVersion = "2.7.5"
   
   
   // Main
   val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
   val logback = "ch.qos.logback" % "logback-classic" % logbackVersion
   val cassandraConnector = "com.datastax.spark" % "spark-cassandra-connector_2.11" % cassandraConnectorVersion
   val woodstox = "com.fasterxml.woodstox" % "woodstox-core" % woodstoxVersion
   val typesafeConfig = "com.typesafe" % "config" % typesafeConfigVersion
   val scalatra = "org.scalatra" % "scalatra_2.11" % scalatraVersion
   val scalatraSwagger = "org.scalatra" % "scalatra-swagger_2.11" % scalatraVersion
   val scalatraJson = "org.scalatra" % "scalatra-json_2.11" % scalatraVersion
   val json4sNative = "org.json4s" % "json4s-native_2.11" % jason4sVersion
   val json4sExt = "org.json4s" % "json4s-ext_2.11" % jason4sVersion
   val phantomDsl = "com.websudos" %% "phantom-dsl" % phatomVersion
   val akka = "com.typesafe.akka" %% "akka-actor" % akkaVersion
   val dispatch = "net.databinder.dispatch" %% "dispatch-core" % dispatchversion
   val quartz = "org.quartz-scheduler" % "quartz" % quartzVersion
   val commonsNet = "commons-net" % "commons-net" % commonsNetVersion
   val commonsIo =  "commons-io" % "commons-io" % commonsIoVersion   
   val jacksonScala = "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % jacksonVersion  
   val jacksonCore = "com.fasterxml.jackson.core" % "jackson-core" % jacksonVersion
   val jacksonJdk8Types = "com.fasterxml.jackson.datatype" % "jackson-datatype-jdk8" % jacksonVersion
   val jacksonAnnotations = "com.fasterxml.jackson.core" % "jackson-annotations" % jacksonVersion

   //Provided
   val sparkCore = "org.apache.spark" % "spark-core_2.11" % sparkVersion % "provided"
   val sparkSql = "org.apache.spark" % "spark-sql_2.11" % sparkVersion % "provided"
   val servletApi = "javax.servlet" % "javax.servlet-api" % servletApiVersion % "provided"
   val jetty = "org.eclipse.jetty" % "jetty-webapp" % jettyVersion % "compile,container"
   
   //  Test
   val scalatest = "org.scalatest" %% "scalatest" % scalatestVersion % "test"
   val scalamock = "org.scalamock" %% "scalamock-scalatest-support" % scalamockVersion % "test"
   
}

