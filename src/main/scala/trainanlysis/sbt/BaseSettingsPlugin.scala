/*
 * Copyright 2016 Chris Martin
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
 
import sbt._
import Keys._
import scoverage.ScoverageKeys._
import org.scalastyle.sbt.ScalastylePlugin._
import com.typesafe.sbteclipse.plugin.EclipsePlugin._

/*
 * SBT Plugin which encapsulates some of the common settings needed across train analysis builds
 * Note that these can be overridden at the project level if need be
 */
object CommonSettingsPlugin extends AutoPlugin {
  
  // Take build number from env (set in Jenkins) or default to dev if it's not there.
  private val buildVersion = sys.env.getOrElse("BUILD_NUMBER", "dev")
  
  override def trigger = allRequirements
    
  override lazy val projectSettings = Seq(
    
    // Base settings
    organization  := "org.cmj",
    version       := buildVersion,
    scalaVersion  := "2.11.7",
    scalacOptions := Seq(
      "-unchecked", "-deprecation", "-encoding", "utf8", "-feature",
      "-language:existentials", "-language:implicitConversions"
    ),

    // Everything Apache Licensed
    licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html")),
    
    // resolvers 
    resolvers += Resolver.jcenterRepo,
    
    // Settings for Eclipse Plugin
    unmanagedSourceDirectories in Compile := (scalaSource in Compile).value :: Nil,
    unmanagedSourceDirectories in Test := (scalaSource in Test).value :: Nil,
    EclipseKeys.createSrc := EclipseCreateSrc.Default,
    EclipseKeys.withSource := true,
    EclipseKeys.withJavadoc := true,
    
    // Add an alias so we can easily run all the commands for ci
    commands += Command.command("ci") {
      "clean" ::
      "package" ::
      "coverage" ::
      "test" ::
      "scalastyle" ::
      "coverageReport" ::
      _
    },

    // Static Analysis
    testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/test-reports"),
    coverageEnabled := true,
    scalastyleConfigUrl := Some(url("https://bitbucket.org/d80tb7/trainanalysis-sbt-plugin/raw/9dc90d13dcb5cf3a5085d11e29ea3d1296e10c7c/src/main/resources/scalastyle-config.xml"))
  )
}
