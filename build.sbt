import org.scalastyle.sbt.ScalastylePlugin._
lazy val `sbt-base-settings` = project in file(".")

name := "trainanalysis-sbt-plugin"
licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))
scalaVersion  := "2.10.4"
sbtPlugin := true
publishMavenStyle := false

// Plugins
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.3.5")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.8.0")
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "4.0.0")
